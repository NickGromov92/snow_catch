class SnowCatch {
	constructor() {
		this.snowFlake = document.getElementById('Capa_1'); // https://learn.javascript.ru/searching-elements-dom#document-getelementbyid-ili-prosto-id
		this.startButton = document.getElementById('startButton');
		this.gameCanvas = document.getElementById('gameCanvas');
		this.amountText = document.getElementById('amountText');
		this.timeText = document.getElementById('timeText');
		this.gameTime = 30;
		this.init();
	}

	init() {
		this.attachEvents();
	}

	attachEvents() {
		this.startButton.addEventListener('click', () => { // https://learn.javascript.ru/introduction-browser-events#addeventlistener
			this.start();
		});
	}

	setSnowRandomPosition() {
		this.snowFlake.style.top = Math.round(Math.random() * 90) + '%'; // https://learn.javascript.ru/styles-and-classes#element-style
		this.snowFlake.style.left = Math.round(Math.random() * 90) + '%';
	}

	start() {
		this.startGameTimer();
		let amount = 0;
		this.snowFlake.style.display = 'block';
		this.gameCanvas.style.opacity = '1';

		this.setSnowRandomPosition();

		this.snowInterval = setInterval(() => { // https://learn.javascript.ru/settimeout-setinterval#setinterval
			this.setSnowRandomPosition();
		}, 1500);

		this.snowFlake.addEventListener('click', () => {
			clearInterval(this.snowInterval);

			this.snowInterval = setInterval(() => {
				this.setSnowRandomPosition();
			}, 1500);

			this.setSnowRandomPosition();
			amount++;
			this.amountText.style.visibility = 'visible';
			this.amountText.textContent = `You caught - ${amount}`; // https://learn.javascript.ru/basic-dom-node-properties#textcontent-prosto-tekst
		});
	}

	startGameTimer() {
		this.timeText.style.visibility = 'visible';
		this.timeText.textContent = `You have ${this.gameTime}`;
		let startTime = new Date().getTime();

		this.gameInterval = setInterval(() => {
			let currentTime = new Date().getTime();
			let difference = Math.round((currentTime - startTime) / 1000);

			let leftTime = this.gameTime - difference;

			if (this.gameTime > difference) {
				this.timeText.textContent = `You have ${leftTime}`;
			} else {
				this.stop();
			}
		}, 1000);
	}

	stop() {
		this.timeText.textContent = 'Time left';
		this.snowFlake.style.display = 'none';
		this.gameCanvas.style.opacity = '.75';
		clearInterval(this.gameInterval);
	}
}

new SnowCatch();
